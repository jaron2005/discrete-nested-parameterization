# nested parameterization

Returns the nested markov paramterization for arbitrary ADMGs with discrete
observed variables. Usage described below; see tests for additional examples.

## Usage

An ADMG is represnted by a sequence where the first item represents directed
edges, and the second represents bidirected edges. Each of these is represented
by a sequence of length two tuples.

For example, the ADMG

```
A -> B -> C -> D -> E, B <-> E, C <-> E
```

can be constructed

```.py
admg = ((('A', 'B'), ('B', 'C'), ('C', 'D'), ('D', 'E')), (('B', 'E'), ('C', 'E')))
```

### Retrieving parameters

Suppose we are interested in the parameterization for the above ADMG with
ternary random variables. We can obtain the parameters of the problem with

```
cardinalities = [(node, 3) for node in set(chain(admg[0], admg[1]))]
parameters = parameterize(admg, cardinalities)
```

### Retrieving expression for outcome probabilities

Suppose we are interested in `P(A = B = C = D = 0, E = 1)`. We can obtain an
expression for this as follows

```py
outcome = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E':1}
heads_tails = get_heads_tails(admg)
expression = outcome_probability(outcome, cardinalities, heads_tails)
```

If we would like all expressions for all outcomes, we can obtain them as follows

```py
expressions = get_probability_expressions(admg, cardinalities)
```

This will produce a map from tuples to probability expressions. The tuples will
represent values taken by variables in the ADMG _in alphabetical order_, i.e.
the first value in the tuple will represent the value taken by the
alphabetically first variable, and so on (this can be easily changed).

### Converting nested markov parameters into probability distributions

Suppose we want to create a distribution corresponding to the nested markov
parameters all taken the value `0.01`. This can be done as follows

```.py
parameter_values = ((str(p), 0.01) for p in parameters)
distribution = get_distribution(parameter_values, cardinalities, admg=admg)
```

If this function will be called many times with different parameter values, as
in the case of optimization, it will make sense to pre-compute the probability
expressions, as above, and to obtain the distribution as follows


```.py
distribution = get_distribution(parameter_values, cardinalities, expressions=expressions)
```

The distribution will be a numeric numpy array with the probability at index
`(idx1, ... idxN)` corresponding to the event where the _alphabetically first_
variable takes the value `idx1`, and so on.

