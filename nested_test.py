from scipy.optimize import minimize
from functools import partial
from nested import *

admg = ((('X', 'E'), ('E', 'M'), ('M', 'Y')), (('E', 'Y'),))
cardinalities = [(v, 2) for v in rchain(admg)]
heads_tails = get_heads_tails(admg)


def test_partition():
    # examples from page 17 of Evans and Richardson 2017
    result = {h for h, t in partition({'X', 'E', 'Y'}, heads_tails)}
    expected = {('X',), ('E', 'Y')}
    assert result == expected

    result = {h for h, t in partition({'M', 'Y'}, heads_tails)}
    expected = {('Y',), ('M',)}
    assert result == expected


def test_get_ys():
    c = {'A', 'C', 'D'}
    o = {'C', 'D'}
    outcome = {'A': 0, 'B': 0, 'C': 1, 'D': 1}
    cardinalities = {v: 2 for v in outcome}
    result = list(get_ys(c, o, outcome, cardinalities))
    assert len(result) == 1
    assert result[0] == {v: 1 for v in c}


def test_factorization():
    # example from page 2 of Evans and Richardson 2017

    # simplest case
    outcome = {v: 1 for v in rchain(admg)}
    result = outcome_probability(outcome, cardinalities, heads_tails)
    expected = sy.Symbol('qEY(E=1,Y=1|M=1,X=1)') * sy.Symbol('qM(M=1|E=1)') * sy.Symbol('qX(X=1)')
    assert result == expected

    # slightly more involved case
    outcome['X'] = 0
    result = outcome_probability(outcome, cardinalities, heads_tails)
    expected = sy.Symbol('qEY(E=1,Y=1|M=1,X=0)') * sy.Symbol('qM(M=1|E=1)') * (1 - sy.Symbol('qX(X=1)'))
    assert result.simplify() == expected.simplify()


def test_parameterize():
    # example from page 2 of Evans and Richardson 2017
    # note we use {X = 0 | X in nodes} as the corner case, rather than X = 1
    result = parameterize(admg, cardinalities)
    assert len(result) == 11
    expected = {
         sy.Symbol('qE(E=1|X=0)'),
         sy.Symbol('qEY(E=1,Y=1|M=0,X=1)'),
         sy.Symbol('qE(E=1|X=1)'),
         sy.Symbol('qEY(E=1,Y=1|M=1,X=1)'),
         sy.Symbol('qY(Y=1|M=1)'),
         sy.Symbol('qM(M=1|E=1)'),
         sy.Symbol('qY(Y=1|M=0)'),
         sy.Symbol('qEY(E=1,Y=1|M=1,X=0)'),
         sy.Symbol('qM(M=1|E=0)'),
         sy.Symbol('qX(X=1)'),
         sy.Symbol('qEY(E=1,Y=1|M=0,X=0)')
    }
    assert result == list(sorted(expected, key=str))


def test_optimization_demo():
    # template for optimizing functional over distributions in nested markov model
    admg = ((('A', 'B'), ('B', 'C'), ('C', 'D'), ('D', 'E')), (('B', 'E'), ('C', 'E')))
    cardinalities = [(v, 2) for v in rchain(admg)]
    params = parameterize(admg, cardinalities)
    expressions = get_probability_expressions(admg, cardinalities)

    def functional(vals):
        def H(p):
            return sum(x * -np.log2(x) for x in p.flatten())

        def I(p):
            return H(p.sum(axis=(1, 2, 3, 4))) + H(p.sum(axis=(0, 2, 3))) - H(p.sum(axis=(2, 3)))

        param_vals = zip([str(p) for p in params], vals)
        dist = get_distribution(param_vals, cardinalities, expressions)
        target = H(np.sum(dist, axis=(0, 2, 3, 4))) - I(dist)
        print(target, dist)
        return target

    def constraint(vals):
        param_vals = zip([str(p) for p in params], vals)
        dist = get_distribution(param_vals, cardinalities, expressions)
        return -(np.abs(np.sum(dist) - 1)) - (np.abs(np.sum(dist[dist < 0])))

    x0 = np.array([0.32**(len(str(p)) / 1.01) for p in params])
    res = minimize(
        functional, x0,
        bounds=[(1e-10, 1 - 1e-10)] * len(params),
        constraints={
            'type': 'ineq',
            'fun': constraint
        }
    )
    print(res)
